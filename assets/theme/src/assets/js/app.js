$(document).foundation();
$(document).ready(function(){
  $('.ticker').slick({
      slidesToShow: 7,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
    });
});
$('.baner-mob').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows:false,
    autoplay: true,
    autoplaySpeed: 5000,});
// ----------------------  Baner Home --------------------------- //

$('#network').hover(function () {
    $('.baner_nav li').css({
        backgroundColor: '#454545',
        transition: 'all .5s ease-in-out'
    });
    $(this).css({
        backgroundColor: '#DA8940',
        transition: 'all .5s ease-in-out'
    });
    $('.baner_content').css({
        backgroundColor: '#DA8940',
        transition: 'all .5s ease-in-out'
    });
    $('.baner').css({
        backgroundImage: "url(assets/theme/dist/assets/img/networking.jpg)",
        transition: 'all .5s ease-in-out'
    });
    $('div.network, div.web, div.repairs, div.software').addClass("hide");
    $('div.network').removeClass("hide");
}, function(){
    $(this).css({
        //backgroundColor: '#454545',
        //transition: 'all .5s ease-in-out'
    });
    //$('div.network').addClass("hide");
    //$('.baner_content').css("background-color", "#DA8940");
});
$('#web').hover(function () {
    $('.baner_nav li').css({
        backgroundColor: '#454545',
        transition: 'all .5s ease-in-out'
    });
    $(this).css({
        backgroundColor: '#38BADB',
        transition: 'all .5s ease-in-out'
    });
    $('div.network, div.web, div.repairs, div.software').addClass("hide");
    $('div.web').removeClass("hide");
    $('.baner_content').css("background-color", "#38BADB");
    $('.baner').css({
        backgroundImage: "url(assets/theme/dist/assets/img/web-solutions.jpg)",
        transition: 'all .5s ease-in-out'
    });
}, function(){
//    $(this).css({
//        backgroundColor: '#454545',
//        transition: 'all .5s ease-in-out'
//    });
//    $('#network').css({
//        backgroundColor: '#DA8940',
//        transition: 'all .5s ease-in-out'
//    });
//    $('div.web').addClass("hide");
//    $('div.network').removeClass("hide");
//    $('.baner_content').css("background-color", "#DA8940");
});

$('#repairs').hover(function () {
    $('.baner_nav li').css({
        backgroundColor: '#454545',
        transition: 'all .5s ease-in-out'
    });
    $(this).css({
        backgroundColor: '#5CB07A',
        transition: 'all .5s ease-in-out'
    });
    $('div.network, div.web, div.repairs, div.software').addClass("hide");
    $('div.repairs').removeClass("hide");
    $('.baner_content').css("background-color", "#5CB07A");
    $('.baner').css({
        backgroundImage: "url(assets/theme/dist/assets/img/repairs.jpg)",
        transition: 'all .5s ease-in-out'
    });
}, function(){
//    $(this).css({
//        backgroundColor: '#454545',
//        transition: 'all .5s ease-in-out'
//    });
//    $('#network').css({
//        backgroundColor: '#DA8940',
//        transition: 'all .5s ease-in-out'
//    });
//    $('div.repairs').addClass("hide");
//    $('div.network').removeClass("hide");
//    $('.baner_content').css("background-color", "#DA8940");
});
$('#software').hover(function () {
    $('.baner_nav li').css({
        backgroundColor: '#454545',
        transition: 'all .5s ease-in-out'
    });
    $(this).css({
        backgroundColor: '#DA5B5B',
        transition: 'all .5s ease-in-out'
    });
    $('#network').css({
        backgroundColor: '#454545',
        transition: 'all .5s ease-in-out'
    });
    $('div.network, div.web, div.repairs, div.software').addClass("hide");
    $('div.software').removeClass("hide");
    $('.baner_content').css("background-color", "#DA5B5B");
    $('.baner').css({
        backgroundImage: "url(assets/theme/dist/assets/img/software.jpg)",
        transition: 'all .5s ease-in-out'
    });
});

// -------- Video popup --------------//

$('#start_video').click(function () {
    $('#start_video').addClass('hide');
    $('#video').removeClass('hide');
    return false;
});

var iWW = $(window).width();
var iWH = $(window).height(); 

$(window).on("load resize", function () {
    $(".baner_curtain").animate({backgroundPosition: -Math.floor(1000-$(window).width()/2)  }, {
    duration: 2000
    });
    $('.video').delay(2000).animate({opacity: 1},{duration: 500});
    $('ul.baner_nav li').each(function(i) {
      $(this).delay(600 * i).animate({marginTop: "0"});
    });
// Invoke the resize event immediately
}).resize();

// mob menu
var mobClick = $(".mob-click");
var mobMenu = $(".mob-menu");

mobClick.click(function(){
            mobMenu.slideToggle(200);
        });
